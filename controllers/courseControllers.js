const Course = require('../models/Course')
const User = require('../models/Course')




// Create new Course
// create new course object using the mongoose model
module.exports.addCourse = (reqBody) => {

	if(reqBody.isAdmin) {
		// Creates a new variable "newCourse" and instantiate a new Course object
		// Uses the info from the request body to provide all necessary info
		let newCourse = new Course({
			name: reqBody.course.name,
			description: reqBody.course.description,
			price: reqBody.course.price
		});

		// saves the created object to our database
		return newCourse.save().then((course, error) => {
			// course creation failed
			if(error){
				
				return false
			// course creation succeed
			} else {
				
				return true
			}
		})
	} else {
		return false
	}	
}

module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result
	})
}

module.exports.getAllActiveCourse = () => {
	return Course.find({ isActive: true }).then(result => {
		return result
	})
}

module.exports.getCourse = (reqParams) => {
	console.log(reqParams)
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course ,err) => {
		if(err) {
			return false
		} else {
			return true
		}    
	})
}

module.exports.archiveCourse = (data) => {

    if (data.isAdmin === true) {
    	let updateActiveField = {
    		isActive : data.reqisActive.isActive
    	}

        return Course.findByIdAndUpdate(data.reqParams.courseId, updateActiveField)
            .then((course, err) => {
                if(err) {
                    return false
                } else {
                    return true
                }
            })
    } else {
        return false
    }
}