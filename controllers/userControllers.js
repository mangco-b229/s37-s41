const User = require('../models/User')
const Course = require('../models/Course')
const auth = require('../auth.js')
const bcrypt = require('bcrypt')
// check email duplicate
/*
	Steps:
	1. use find method for duplicate emails
	2. use the "then" method
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}
// user registration
// create new User object
// make sure that the password is encrypted
// save the new User 
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10) //10 saltrounds
	})

	return newUser.save().then((user, error) => {
		// registration failed
		if(error) {
			return false
		} else {
			return true
		}
	})
}

// check email exists
// compare password
// generate acess token


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAcessToken(result)}
			} else {
				return false
			}
		}
	})
}

// get user profile


module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.userId).then(result => {
		console.log(result)

		result.password = ""

		return result
	})
}

// Enroll

/*
	1. Find User to be updated
	2. Add the course id to user's enrollment array
	3. Update the document
*/

// async-await is needed to update two separate documents
module.exports.enroll = async (data) => {

	// await will allow the enroll method to  complete updating the user before returning a response back to the frontend
	let isUserUpdated = await User.findById(data.userToken).then(user => { 
		user.enrollments.push({ courseId: data.courseId })

		return user.save().then((user, err) => {
			if(err) {
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {		
		course.enrollees.push({ userId: data.userToken })

		return course.save().then((course, err) => {
			if(err) {
				return false
			} else {
				return true
			}
		})
	})

	if(isUserUpdated && isCourseUpdated) {
		return true
	} else {
		return false
	}
}