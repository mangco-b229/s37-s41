const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoute = require('./routes/userRoute')
const courseRoute = require('./routes/courseRoute')

const app = express()

mongoose.set('strictQuery', true)

mongoose.connect("mongodb+srv://admin:admin1234@cluster0.n5pfk4s.mongodb.net/S37-S41?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology: true
});

let db = mongoose.connection

db.on("error", console.error.bind(console, "Connection Error"))

db.once("open", () => console.log("We're connected to the cloud database."));

// MIDDLEWARES
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/users', userRoute)
app.use('/courses', courseRoute)

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})