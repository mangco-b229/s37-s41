const express =  require('express')
const router = express.Router()
const courseControllers = require('../controllers/courseControllers')
const auth = require('../auth')


//create course
router.post('/', (req, res) => {
	
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseControllers.addCourse(data)
		.then(resultFromController => res.send(resultFromController))
})

router.get('/all', (req, res) => {
	courseControllers.getAllCourse()
		.then(resultFromController => res.send(resultFromController))
})

router.get('/', (req, res) => {
	courseControllers.getAllActiveCourse()
		.then(resultFromController => res.send(resultFromController))
})

router.get('/:courseId', (req, res) => {
	courseControllers.getCourse(req.params)
		.then(resultFromController => res.send(resultFromController))	
})

router.put("/:courseId", auth.verify, (req, res) => {
	courseControllers.updateCourse(req.params, req.body)
		.then(resultFromController => res.send(resultFromController))
})


router.put('/:courseId/archive', auth.verify, (req, res) => {

    const data = {
    	reqParams: req.params,
    	reqisActive: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    courseControllers.archiveCourse(data)
        .then(resultFromController => res.send(resultFromController))

})

module.exports = router