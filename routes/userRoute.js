const express = require('express')
const router = express.Router()
const userControllers = require('../controllers/userControllers')
const auth = require('../auth')

//Check email duplicates

router.post('/checkemail', (req, res) => {
	userControllers.checkEmailExists(req.body)
		.then(resultFromController => res.send(resultFromController))
})

// User Registration route
router.post('/register', (req, res) => {
	userControllers.registerUser(req.body)
		.then(resultFromController => res.send(resultFromController))		
})

// user login
router.post('/login', (req, res) => {
	userControllers.loginUser(req.body)
		.then(resultFromController => res.send(resultFromController))
})

//retrieving user details
router.post('/details', auth.verify, (req, res) => {
	// uses the decode method defined inthe auth.js to treieve user info from req header
	const userData = auth.decode(req.headers.authorization)

	userControllers.getProfile({ userId: req.body.id })
		.then(resultFromController => res.send(resultFromController))
})

router.post('/enroll', auth.verify, (req, res) => {
	let data = {
		courseId: req.body.courseId,
		userToken: auth.decode(req.headers.authorization).id
	}

	userControllers.enroll(data)
		.then(resultFromController => res.send(resultFromController))	
})

module.exports = router 			